import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class FootballClub(BaseModel):
    name: str
    ClubPresiden: str
    NumberOfPlayers: int

class Coach(BaseModel):
    nameCoach: str
    lastnameCoach: str
    yersCoach: int
    yearsOfCoaching: int
    teamOfCoaching: int
    winTotalsGames: int
    nameWonCups: str
    wonCups: int

class Doctor(BaseModel):
    nameDoctor: str
    lastnameDoctor: str
    yersDoctor: int
    doctorExperience: int

class Player(BaseModel):
    name: str
    lastname: str
    position: str
    numberPlayer: int
    gamesPlayed: int
    passAll: int
    shoutAll: int
    save: int
    goal: int


@app.put("/players/{player_id}")
def update_player(player_id: int, player: Player):
    return {"player": player.name, "player_id": player_id}

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)